import json
from pprint import pprint
import os
import time

class Color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

class Elements:
    def __init__(self, name, symbol, group, atomicNumber, molarMass, electroNegativity):#I will be adding more variables per element as I see necessary
        self.__name = name
        self.__symbol = symbol
        self.__group = group
        self.__atomicNumber = atomicNumber
        self.__molarMass = molarMass
        self.__electroNegativity = electroNegativity

    #setters: I wrote it before realizing that they are not really necessary since the variables are constants. Perhaps, I could implement
    #a function later that allows the user to input another more precise value for the calculations. ie, molar mass with larger mantissa.
    def set_name(self, name):
        self.__name = name #string name element

    def set_symbol(self, symbol):
        self.__symbol = symbol #stringh symbol (can be one or two characters)

    def set_group(self, group):
        self.__group = group

    def set_molar_mass(self, molarMass):
        self.__molarMass = molarMass  # molar mass is a double

    def set_atomic_number(self, atomicNumber):
        self.__atomicNumber =  atomicNumber

    def set_electro(self, electroNegativity):
        self.__electroNegativity = electroNegativity

    #getters: Al variables are private so this methods can access to the variables to be shown outside of the class as requested.
    def getName(self):
        return  self.__name

    def getGroup(self):
        return self.__group

    def getSymbol(self):
        return self.__symbol

    def getMolarMass(self):
        return float(self.__molarMass)

    def getAtomicNumber(self):
        return self.__atomicNumber

    def getElectroN(self):
        return self.__electroNegativity


#Creates
class Table:
    def __init__(self):
        self.__table = []
        self.jSon()
        assert len(self.__table) == 118 #Verify that the list contains only 118 elements.

    def jSon(self):#Use a Json file to pull out the information.
        with open('data.json') as f, open ('file.txt', 'w') as a:

            data = json.load(f)

            i=0
            while(i<118):#Remember that every element is 1 index under its atomic number. e.g., Hydrogen
                         #has an atomic number of 1 and it is listed as the first elemen/Users/antonio/Documents/CIS407/projectcalculatort, but in the
                         #table is storage in the index 0.
                self.__table.append(Elements(data[i]["name"], data[i]["symbol"],
                                             data[i]["groupBlock"], data[i]["atomicNumber"],
                                             data[i]["atomicMass"], data[i]["electronegativity"]))
                linea = "{0} {1} {2} {3} {4} \n".format(data[i]["name"], data[i]["symbol"],
                                             data[i]["atomicNumber"],
                                             data[i]["atomicMass"], data[i]["electronegativity"])

                a.write(linea)
                i+=1



    def getTable(self):
        return self.__table#It returns the list with the elements.

class Formulas:
    '''
    Tentative name. It will be the class that performs the operations. It will return the results of the calculations to be used
    in the main application and it will be possible to be used in other people projects.
    '''

    def __init__(self):
        self.__nuevo = Table()
        self.__mesa = self.__nuevo.getTable()
        self.__resultados = []

    def saveTolist(self, resultado):#for saving calculations
        self.__resultados.append(resultado)

    def getSave(self):#for printing calculations
        return self.__resultados

    def lookUpName(self, index):
        return self.__mesa[index].getName()

    def getFormula(self, metodo):# verificar es correcta!
        #metodo indicates how the formula shuld be verify.
        valido = 0
        again = 0
        emptySpace = False
        formuFinal =  []

        if metodo is 1:#sort

            while valido == 0:
                if again is 1:
                    formuFinal = []
                    print("Bad imput. Please, try again.")

                emptySpace = False
                while emptySpace == False:#check if input is empty
                    formu = input().split();#Gets the formula and split it in an array.
                    if len(formu) != 0:
                        emptySpace = True
                    else:
                        print("The imput is empty!.")

                valido = 1
                i = 0
                while i<len(formu):

                    if self.elementSearch(formu[i]) != -1:
                        formuFinal.append(formu[i])
                    else:
                        valido = 0
                        again = 1
                        break
                    i+=1


        elif metodo is 2:#molar mass
            while valido == 0:
                if again is 1:
                    formuFinal = []
                    print("Bad imput. Please, try again.")

                emptySpace = False
                while emptySpace == False:
                    formu = input().split();#Gets the formula and split it in an array.
                    if len(formu) != 0:
                        emptySpace = True
                    else:
                        print("The imput is empty!.")

                valido = 1
                length = len(formu)

                i = 0
                while i<length:
                    if self.elementSearch(formu[0]) == -1:
                        valido = 0
                        again = 1
                        break

                    if i+1<length:
                        if (self.elementSearch(formu[i]) != -1) and (self.elementSearch(formu[i+1]) != -1):
                                formuFinal.append(formu[i])
                                formuFinal.append('1')

                        elif self.elementSearch(formu[i]) != -1:
                            formuFinal.append(formu[i])

                        elif formu[i].isdigit() == True:
                            formuFinal.append(formu[i])

                    else:
                        if self.elementSearch(formu[i]) != -1:
                            formuFinal.append(formu[i])
                            formuFinal.append('1')

                        elif formu[i].isdigit() == True:
                            formuFinal.append(formu[i])
                        else:
                            valido = 0
                            again =1
                    i+=1

        return formuFinal


    def isFloat(self, grams):#To check if a value is float n  based in https://stackoverflow.com/questions/736043/checking-if-a-string-can-be-converted-to-float-in-python
        try:
            float(grams)
            return True
        except ValueError:
            return False

    def elementSearch(self, elemento):#look for the index in the periodic table(by symbol) that holds the element.
    #It search the element
        i = 0
        length = len(self.__mesa)

        while i<length:
            if self.__mesa[i].getSymbol() == elemento:
                return i
            i+=1#returns the index of the location

        return -1#case is not found! It helps to detect if the formula is wrong.

    #Calculate the molar mass when a formula is provided
    def molarMassCalc(self, chemForm):#(It need to have as an integer for every element of the formula. For instance, H2O would need to be introduce as H2O1 to be processed // updated. H 2 O can be entered.)
        coeficiente = 0
        masaElemento = 0

        i = 0
        length = len(chemForm)
        total = 0

        while i<length:
            masaElemento = self.__mesa[self.elementSearch(chemForm[i])].getMolarMass()
            coeficiente = int(chemForm[i+1])

            temp = masaElemento * coeficiente

            total = float(total + temp)
            i= i + 2

        return total #Once called it will return a float with the result.

    #Allow us to sort elements we provide by MolarMass, Atomic Number, electroNegativity
    def sortElements(self, field, chemForm):

        formulaNeg = chemForm.copy()
        formulaVal = []

        pivotOne = 0
        length = len(formulaNeg)

        if field == 1:#molar mass
            while (pivotOne<length):
                pivotTwo = pivotOne + 1

                while(pivotTwo<length):
                    if self.__mesa[self.elementSearch(formulaNeg[pivotOne])].getAtomicNumber() > self.__mesa[self.elementSearch(formulaNeg[pivotTwo])].getAtomicNumber():
                        formulaNeg[pivotOne], formulaNeg[pivotTwo] = formulaNeg[pivotTwo] , formulaNeg[pivotOne]
                    pivotTwo+=1
                pivotOne+=1

            for x in formulaNeg:
                formulaVal.append(self.__mesa[self.elementSearch(x)].getSymbol())
                formulaVal.append(self.__mesa[self.elementSearch(x)].getAtomicNumber())

        elif field == 2:
            while (pivotOne<length):
                pivotTwo = pivotOne + 1
                while(pivotTwo<length):

                    if self.__mesa[self.elementSearch(formulaNeg[pivotOne])].getMolarMass() > self.__mesa[self.elementSearch(formulaNeg[pivotTwo])].getMolarMass():
                        formulaNeg[pivotOne], formulaNeg[pivotTwo] = formulaNeg[pivotTwo] , formulaNeg[pivotOne]
                    pivotTwo+=1

                pivotOne+=1

            for x in formulaNeg:
                formulaVal.append(self.__mesa[self.elementSearch(x)].getSymbol())
                formulaVal.append( str(round(self.__mesa[self.elementSearch(x)].getMolarMass(),2))   +"g/mol")

        elif field == 3:
            while (pivotOne<length):
                pivotTwo = pivotOne + 1
                while(pivotTwo<length):

                    if self.__mesa[self.elementSearch(formulaNeg[pivotOne])].getElectroN() > self.__mesa[self.elementSearch(formulaNeg[pivotTwo])].getElectroN():
                        formulaNeg[pivotOne], formulaNeg[pivotTwo] = formulaNeg[pivotTwo] , formulaNeg[pivotOne]
                    pivotTwo+=1

                pivotOne+=1

            for x in formulaNeg:
                formulaVal.append(self.__mesa[self.elementSearch(x)].getSymbol())
                formulaVal.append(round(self.__mesa[self.elementSearch(x)].getElectroN(),2))

        return formulaVal;

    def gramsToMoles(self, grams, chemForm):#check error if grams not a number
    #Number of moles is equal to the mass of the compound (gr) over molar mass of the compound
        if self.isFloat(grams)==True:
            formulaMoles = float(grams) / self.molarMassCalc(chemForm)
        elif grams.isdigit() == True:
            formulaMoles = int(grams) / self.molarMassCalc(chemForm)
        else:
            return -1  #error!

        return formulaMoles

    def molesToGrams(self, moles, chemForm):
        #moles to grams, provide the number of moles and the compound and we get the weight.
        if self.isFloat(moles)==True:
            formulaGrams = float(moles) * self.molarMassCalc(chemForm)
        elif grams.isdigit() == True:
            formulaGrams = int(moles) * self.molarMassCalc(chemForm)

        return formulaGrams


    def molaritySolution(self, grams, liters, chemForm):

        numeroDeMoles = self.gramsToMoles(grams, chemForm)
        molarityInSolution = numeroDeMoles / float(liters)

        return molarityInSolution

    def howManyGramsMolarity (self, molarity, liters, chemForm):

        molesSolute = float(molarity) * float(liters)
        gramsWeight = molesSolute * self.molarMassCalc(chemForm)

        return gramsWeight

    def massPercentage(self, mass, chemForm):#fuction that gets compound and separate in weights and percentage
    #returns three lists, 1, the synbols of the elements, 2, how many moles/grams, 3 the percentage/

        totalMass = self.molarMassCalc(chemForm)

        percentageList = []#holds the percentage amount of an element
        massElementPresent = []#how many molar mass of an element
        elementList = []
        molarMassTotalPercentage = self.molarMassCalc(chemForm)
        i = 0
        while i < len(chemForm):
            temp = self.__mesa[self.elementSearch(chemForm[i])].getMolarMass()
            temp *= int(chemForm[i+1])
            elementList.append(chemForm[i])
            value = temp/molarMassTotalPercentage
            percentageList.append(round((value)*100, 2))
            massElementPresent.append(round((float(mass)*value),2))
            #percentageList,append(roound((temp/molarMassTotalPercentage)*100))
            i+=2

        return elementList, massElementPresent, percentageList;

    #theorical yield

    #molarity
