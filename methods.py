import os
from color import *

def menu(option):

    if option == 1:#main
        print(" Chemical calculator. Please, select what operation you would like to perform:")
        print(" * Press '1' for calculate the molar mass.")
        print(" * Press '2' for sorting elements by molar mass, atomic number or electronegativit.")
        print(" * Press '3' to transform from moles to grams or grams to moles of a compound.")
        print(" * Press '4' to obtain the mass composition of a compound.")
        print(" * Press '5' to Calculate the Molarity and how many grams of a compound contains. ")
        print(" * Press '6' to check all the calculations you have saved.")
        print(" * 'Q' to quit")
    elif option == 2:#sorting
        #def menuSort():
        print("Please select the sorting method: ")
        print(" * Press '1' : by atomic number.")
        print(" * Press '2' : for atomic mass.")
        print(" * Press '3' : for electronegativity.")
        print(" * Press 'R' to go back.")
    elif option == 3:#save
        #def menuSave():
        print(" * Press 'Y' for new molar calculation.\n * Press 'S' to save the result and continue.\n * Press 'W' to save and return to menu.\n * Press 'R' to return to the main menu without saving.")
    elif option == 4:# moles
        #def menuMoles():
        print("Select:\n * Press 'G' for grams to moles. \n * Press 'M' for moles to grams.\n * Press 'R' other character to go back.")
    elif option == 5:
        #menu molarity
        print("Please, choose between the following options.")
        print(" * Press '1' if you want to know the molarity of a solution. ")
        print(" * Press '2' if you want to know how many grams of a compound is present in a solution.")
        print(" * Press 'R' to go back.")

def clearScreen():#fuction to check if it windows or linux
    if os.name == 'nt':
        os.system('cls')
    else:
        os.system('clear')

def isFloat(value):#To check if a value is float n  based in https://stackoverflow.com/questions/736043/checking-if-a-string-can-be-converted-to-float-in-python
    try:
        float(value)
        return True
    except ValueError:
        return False

def printFormula(array):#To show the name in the fuctions.
    arrayFinal = []
    for x in array:
        if x != '1':
            arrayFinal.append(x)
    return ''.join(arrayFinal)

def optionSave(operations, save):
    menu(3)
    againTodo = True
    while againTodo == True:
        again = input()
        if again == 'Y' or again =='y':#continue without saving
            again = 'Y'
            againTodo = False
        elif again == 'S' or again =='s':#save and continue
            again = 'S'
            againTodo = False
        elif again == 'W' or again == 'w':#save and exit
            again = 'W'
            againTodo = False
        elif again == 'r' or again == 'R':#exit without saving
            again = 'R'
            againTodo = False
        else:
            print("Wrong option. Try again.")
            againTodo = True
    if again == 'Y' or again =='y':
        againOption = True
    elif again == 'S' or again =='s':
        againOption = True
        operations.saveTolist(save)
    elif again == 'W' or again =='w':
        againOption = False
        operations.saveTolist(save)
    elif again == 'R'  or again == 'r':
        againOption = False

    return againOption

def saveToTextFile(imprimir):
    with open('result.txt', 'w') as f:
        i = 0
        for x in imprimir:
            f.write(x)
            f.write('\n')

def printStorage(imprimir):
    color = Color()
    print(color.BLUE)
    for x in imprimir:
        print(x)
    print(color.END)

def commonOperationsOfSort(operations, sort):#common operations for sorting.
    finalString = ''
    chemFormSort = operations.getFormula(1)
    sortedValues = operations.sortElements(int(sort), chemFormSort)
    print()

    i = 0
    while i < len(sortedValues):
        finalString += sortedValues[i]+ "(" + str(sortedValues[i+1]) + ")"
        if i+2 < len(sortedValues):
            finalString += " -> "
        i+=2

    return finalString;

def verifyValue():# checks if it is a numeric value
    correcto = True
    while correcto == True:
        number = input()
        if number.isdigit() != True and isFloat(number)!=True:
            print("The input must be a number. Please, try again. ")
        else:
            break

    return number

def printerResultAndSave(operations, saveResult, printResult):
    print(printResult)#print the result
    print()
    again  = optionSave(operations, saveResult)#save the result
    return again
