import json
from elements import *

class Table:
    def __init__(self):
        self.__table = []
        self.jSon()
        assert len(self.__table) == 118 #Verify that the list contains only 118 elements.

    def jSon(self):#Use a Json file to pull out the information.
        with open('data.json') as f, open ('file.txt', 'w') as a:

            data = json.load(f)

            i=0
            while(i<118):#Remember that every element is 1 index under its atomic number. e.g., Hydrogen
                         #has an atomic number of 1 and it is listed as the first elemen/Users/antonio/Documents/CIS407/projectcalculatort, but in the
                         #table is storage in the index 0.
                self.__table.append(Elements(data[i]["name"], data[i]["symbol"],
                                             data[i]["groupBlock"], data[i]["atomicNumber"],
                                             data[i]["atomicMass"], data[i]["electronegativity"]))
                linea = "{0} {1} {2} {3} {4} \n".format(data[i]["name"], data[i]["symbol"],
                                             data[i]["atomicNumber"],
                                             data[i]["atomicMass"], data[i]["electronegativity"])

                a.write(linea)
                i+=1

    def getTable(self):
        return self.__table#It returns the list with the elements.
