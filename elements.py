class Elements:
    def __init__(self, name, symbol, group, atomicNumber, molarMass, electroNegativity):#I will be adding more variables per element as I see necessary
        self.__name = name
        self.__symbol = symbol
        self.__group = group
        self.__atomicNumber = atomicNumber
        self.__molarMass = molarMass
        self.__electroNegativity = electroNegativity

    #setters: I wrote it before realizing that they are not really necessary since the variables are constants. Perhaps, I could implement
    #a function later that allows the user to input another more precise value for the calculations. ie, molar mass with larger mantissa.
    def set_name(self, name):
        self.__name = name #string name element

    def set_symbol(self, symbol):
        self.__symbol = symbol #stringh symbol (can be one or two characters)

    def set_group(self, group):
        self.__group = group

    def set_molar_mass(self, molarMass):
        self.__molarMass = molarMass  # molar mass is a double

    def set_atomic_number(self, atomicNumber):
        self.__atomicNumber =  atomicNumber

    def set_electro(self, electroNegativity):
        self.__electroNegativity = electroNegativity

    #getters: Al variables are private so this methods can access to the variables to be shown outside of the class as requested.
    def getName(self):
        return  self.__name

    def getGroup(self):
        return self.__group

    def getSymbol(self):
        return self.__symbol

    def getMolarMass(self):
        return float(self.__molarMass)

    def getAtomicNumber(self):
        return self.__atomicNumber

    def getElectroN(self):
        return self.__electroNegativity
