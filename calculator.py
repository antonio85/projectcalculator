'''
Antonio Silva Paucar
CIS 407
Winter 2019

This project will focus in the creation of some libraries that could be used in other python applications.
The data is pulled from json files that were found on the internet and It will allow to create
objects that can perform operations with the chemical formulas provided.

Uses Python 3.7!!!!
Sublime text as main IDE.
Final
'''
from pprint import pprint
import os
import time
from formulas import *
from methods import *
#methods for main

#MAIN
def main():

    operations = Formulas()
    color = Color()

    again = '1'
    #menu of operations.
    option = None
    while again:
        clearScreen()
        menu(1)
        print("Option #: ")
        option = input()

        if option == '1':#molar mass
            againMolar = True
            while againMolar == True:
                clearScreen()
                print("Introduce the chemical formula which Molar Mass you want to be calculated\n(For example, NaCl would need to be input as 'Na Cl', with a space between).")
                exception = True
                while exception == True:
                    try:
                        formChemMolar = operations.getFormula(2)
                        peso = operations.molarMassCalc(formChemMolar)
                        exception = False
                    except:
                        print("Please, check the formula and re-enter it. ")
                        exception = True
                imprimirPeso =  printFormula(formChemMolar)
                saveMolar = "The molar mass of {0} is {1:.2f} g/mol.".format(imprimirPeso, peso)
                printMolar = "The molar mass of {0}{1}{2} is {3}{4:.2f}{5} g/mol.".format(color.RED,imprimirPeso,color.END, color.RED, peso, color.END)
                print()
                againMolar = printerResultAndSave(operations, saveMolar, printMolar)

        elif option == '2':
            #Case we want to order values
            clearScreen()
            againSort =True
            while againSort == True:
                entra = True#to check right option
                menu(2)
                while entra == True:
                    clearScreen()
                    menu(2)
                    sorting = input() #to stop loop
                    if sorting == '1':
                        clearScreen()
                        print("You have selected to sort by atomic number. Please, introduce the symbols of the elements you want to sort.")
                        values = commonOperationsOfSort(operations, sorting)
                        clearScreen()
                        saveSort = "The ascending order by atomic numbers is: {0} (with their atomic number values).".format(values)
                        printSort = "The ascending order by atomic numbers is: {0}{1}{2} (with their atomic number values).".format(color.RED, values, color.END)
                        entra = printerResultAndSave(operations, saveSort, printSort)

                    elif sorting == '2':
                        clearScreen()
                        print("You have selected to sort by molar mass. Please, introduce the symbols of the elements you want to sort. ")
                        values = commonOperationsOfSort(operations, sorting)
                        clearScreen()
                        saveSort ="The ascending order by molar masses is: {0} (with their molar mass values ).".format(values)
                        printSort ="The ascending order by molar masses is: {0}{1}{2} (with their molar mass values ).".format(color.RED, values, color.END)
                        entra = printerResultAndSave(operations, saveSort, printSort)

                    elif sorting == '3':
                        clearScreen()
                        print("You have selected to sort by Electronegativity. Please, introduce the symbols of the elements you want yo sort. ")
                        values = commonOperationsOfSort(operations, sorting)
                        clearScreen()
                        saveSort ="The ascending order by electronegativity is: {0} (with their electroNegativity values.)".format(values)
                        printSort ="The ascending order by electronegativity is: {0}{1}{2} (with their electroNegativity values.)".format(color.RED, values, color.END)
                        entra = printerResultAndSave(operations, saveSort, printSort)

                    elif sorting == 'R' or sorting == 'r':
                        entra = False
                        againSort = False
                        formated = ""
                    else:
                        print("Wrong Input. Try again.\n")
                        time.sleep(1)
                        clearScreen()
                        entra = True

        elif option == '3':
            againMolarGrams = True
            while againMolarGrams == True:
                errorMolar = True
                while errorMolar == True:
                    clearScreen()#clean screen
                    menu(4)
                    gramsOrMoles =  input()

                    if gramsOrMoles == 'G' or gramsOrMoles == 'g':
                        clearScreen()
                        print("Introduce the mass of the compound (in grams) and the formula of the compound. \n")
                        print("Mass in grams: ")
                        massGrams = verifyValue() #verify it is a value
                        print("Now, provide the formula of the compound.")
                        exception = True
                        while exception == True:
                            try:
                                chemFormulaMoles = operations.getFormula(2)
                                numberOfMoles = operations.gramsToMoles(massGrams, chemFormulaMoles)
                                exception = False
                            except:
                                print("Please, check the formula and try again. ")
                                exception = True

                        imprimirMolar =  printFormula(chemFormulaMoles)#gets the formula of the compound
                        print()
                        saveMoles = "The number of moles in {0} g of {1} is {2:2.3f} moles.".format(massGrams,imprimirMolar,numberOfMoles)
                        printMoles = "The number of moles in {0}{1}{2} g of {3}{4}{5} is {6}{7:2.3f}{8} moles.".format(color.RED,massGrams,color.END , color.RED,imprimirMolar,color.END, color.RED, numberOfMoles, color.END)
                        print()
                        errorMolar = printerResultAndSave(operations, saveMoles, printMoles)

                    elif gramsOrMoles == 'M' or gramsOrMoles == 'm':
                        clearScreen()
                        print("Please, introduce the number of Moles and the formula of the compound. ")
                        print("# of Moles: ")
                        molesNumber = verifyValue()#verify it is a value
                        print("Now, provide the formula of the compound.")
                        exception = True
                        while exception == True:
                            try:
                                chemFormulaMoles = operations.getFormula(2)
                                weightInGrams = operations.molesToGrams(molesNumber, chemFormulaMoles)
                                exception = False
                            except:
                                print("Please, check the formula and try again. ")
                                exception = True

                        imprimirMolar =  printFormula(chemFormulaMoles)#gets the formula of the compound
                        saveMoles = "The weight of {0} moles of {1} is {2:2.3f} grams.".format(molesNumber,imprimirMolar,weightInGrams)
                        printMoles = "The weight of {0}{1}{2} moles of {3}{4}{5} is {6}{7:2.3f}{8} grams.".format(color.RED, molesNumber, color.END, color.RED, imprimirMolar, color.END, color.RED, weightInGrams, color.END)
                        print()
                        errorMolar = printerResultAndSave(operations, saveMoles, printMoles )

                    elif gramsOrMoles == 'R' or gramsOrMoles == 'r':
                        errorMolar = False
                        againMolarGrams = False

                    else:
                        print("Bad option!")
                        time.sleep(1)
                        errorMolar = True

        elif option == '4':
            againComposition = True
            while againComposition:
                clearScreen()
                print("Compound percentage composition.\nFirst, Enter the mass of the compound (in grams) to analize its percentage of elements:")
                massComposition = verifyValue()#verify it is a value
                print("Now, provide the formula of the compound: ")
                exception = True
                while exception == True:
                    try:
                        chemFormuComposition = operations.getFormula(2)
                        elementList, molarMassPresent, percentageList = operations.massPercentage(massComposition, chemFormuComposition)
                        exception = False
                    except:
                        print("Please, check the formula and try again. ")
                        exception = True

                formulaName = printFormula(chemFormuComposition)
                #The composition of 100 g H 2 O is 2% (23g) of Hydrogen and 98% (23g) of Oxygen
                text = ''
                i = 0
                while i < len(elementList):
                    text+= str(percentageList[i]) + "% (" + str(molarMassPresent[i]) + "g) of " + operations.lookUpName(operations.elementSearch(elementList[i]));
                    if i == len(elementList)-2:
                        text+=" and "
                    elif i == len(elementList)-1:
                        text+="."
                    else:
                        text+= ", "
                    i+=1

                saveCompo = "The corresponding composition of {0}g of {1} is {2}".format(massComposition, formulaName, text)
                printConvo = "The corresponding composition of {3}{0}g{4} of {3}{1}{4} is {3}{2}{4}".format(massComposition, formulaName, text,color.RED, color.END)
                print()
                againComposition = printerResultAndSave(operations, saveCompo, printConvo)

        elif option == '5':#molarity
            againMolarityP = True
            while againMolarityP == True:
                correct = True
                while correct == True:
                    clearScreen()
                    menu(5)#menu molarity
                    optionMolarity = input()

                    if optionMolarity == '1':
                        clearScreen()
                        print("Find the molarity. ")
                        print("Please, indicate the volumen of the solution(in Liters):")
                        volumen = verifyValue()
                        print("Now, indicate mass of the solute(in Grams):")
                        massSolute = verifyValue()
                        print("Finaly, indicate the chemical Formula:")
                        exception = True
                        while exception == True:
                            try:
                                chemFormMolarity = operations.getFormula(2)
                                molarityTotal = operations.molaritySolution(massSolute,volumen, chemFormMolarity)
                                exception = False
                            except:
                                print("Please, check the formula and try again. ")
                                exception = True
                        formula = printFormula(chemFormMolarity)
                        correct = False
                        saveMolarity = "The molarity of the {0} solution is {1}M.".format(formula, round(molarityTotal, 3))
                        printMolarity = "The molarity of the {2}{0}{3} solution is {2}{1}M{3}.".format(formula, round(molarityTotal, 3), color.RED, color.END)
                        print()
                        againMolarityP = printerResultAndSave(operations, saveMolarity, printMolarity )

                    elif optionMolarity == '2':
                        clearScreen()
                        print("Find the mass of solute. ")
                        print("Please, indicate the volumen of the solution(in Liters):")
                        volumen = verifyValue()
                        print("Now, indicate molarity(M) of the solution:")
                        molaritySolution = verifyValue()
                        print("Finaly, indicate the chemical Formula:")
                        exception = True
                        while exception == True:
                            try:
                                chemFormMolarity = operations.getFormula(2)
                                molarityTotal = operations.howManyGramsMolarity(molaritySolution,volumen, chemFormMolarity)
                                exception = False
                            except:
                                print("Please, check the formula and try again. ")
                                exception = True

                        formula = printFormula(chemFormMolarity)
                        correct = False
                        saveMassSolute = "The weight of solute in {0}M of a solution of {1} is {2}g.".format(molaritySolution, formula, round(molarityTotal, 3))
                        printMassSolute = "The weight of solute in {3}{0}M{4} of a solution of {3}{1}{4} is {3}{2}g{4}.".format(molaritySolution, formula, round(molarityTotal, 3), color.RED, color.END)
                        print()
                        againMolarityP = printerResultAndSave(operations, saveMassSolute, printMassSolute )

                    elif optionMolarity == 'r' or optionMolarity == 'R':
                        correct = False
                        againMolarityP = False
                    else:
                        correct = True

        elif option == '6':#Save the data. It has a delay of 1 second.
            clearScreen()
            imprimir = operations.getSave()
            printStorage(imprimir)
            print("Press 'P' to output the result in a text file or Any key go back to main menu.")
            anyKey = input()
            if anyKey == 'P' or anyKey == 'p':
                print('Save!')
                print("Save! It's located in the source directory")
                saveToTextFile(imprimir)
                time.sleep(1)

        elif (option is 'q') or (option is 'Q'):#Close the program.
            clearScreen()
            again = 0

    return 0

if __name__ == '__main__':
    main()
